/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "oscillatorwidget.h"

OscillatorWidget::OscillatorWidget(Nodule* oscillator): NoduleWidget(oscillator)
{
    SetSizeBlocks(*defaultSize[NT_OSCILLATOR]);
    UpdateNumPorts();
}

void OscillatorWidget::SetPolynomial(const Polynomial& polynomial)
{
    nodule_->SetPolynomial(polynomial);
    UpdateNumPorts();
}

void OscillatorWidget::UpdateNumPorts()
{
    if (!nodule_)
        return;

    const unsigned in{ nodule_->GetNumInputs() };
    const unsigned out{ nodule_->GetNumOutputs() };

    SetSizeBlocks({ Max(in - 1u, out + 1u), 2u });
}

PODVector<IntVector2> OscillatorWidget::GetInputCoords() const
{
    if (!nodule_)
        return {};

    PODVector<IntVector2> coords{ NoduleWidget::GetInputCoords() };

    for (unsigned i{ 0 }; i < nodule_->GetNumInputs(); ++i)
    {
        const int x{ i == 0 ?  0 : -1 };
        const int y{ i == 0 ? -1 :  0 };
        coords.At(i) += IntVector2{ x, y };
    }

    return coords;
}

PODVector<IntVector2> OscillatorWidget::GetOutputCoords() const
{
    return { NoduleWidget::GetOutputCoords().Front() + IntVector2::RIGHT };
}
