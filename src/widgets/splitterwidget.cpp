/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "splitterwidget.h"

PODVector<Unconnection> SplitterWidget::unconnections_{};

SplitterWidget::SplitterWidget(Nodule* splitter): NoduleWidget(splitter)
{
    SetSizeBlocks(*defaultSize[NT_SPLITTER]);
}

void SplitterWidget::DrawPorts(Painter* p) const
{
    bool undefined{ !HasInput() };

    p->DrawEllipse(Vector2{ GetPosition() },
                   Vector2{ IntVector2::ONE * NODULE_PORTSIZE },
                   (undefined ? PortColor(PORT_UNDEFINED) : PortColor(PORT_OUT)) FADE);
}

Port SplitterWidget::PortAt(const IntVector2& coords) const
{
    if (coords_ != coords || dragged_)
        return { PORT_ALL, M_MAX_UNSIGNED };
    else
        return HasInput() ? Port{ PORT_OUT, 0 }
                          : Port{ PORT_UNDEFINED, 0 };
}
