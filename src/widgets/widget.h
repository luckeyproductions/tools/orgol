/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef WIDGET_H
#define WIDGET_H

#include "../gui.h"
#include "widgetdefs.h"

static const Color PortColor(PortType type)
{
    switch (type) {
    case PORT_IN:        return Color::GREEN;  break;
    case PORT_OUT:       return Color::YELLOW; break;
    case PORT_UNDEFINED: return Color::BLUE;   break;
    case PORT_ALL:       return Color::WHITE;  break;
    }
};

class Widget
{
public:
    Widget();
    virtual ~Widget() = default;

    unsigned GetId();
    virtual void Draw(Painter* p) const {}

    IntVector2 GetSize() const { return size_; }
    void SetSize(const IntVector2& size) { size_ = size; }

    IntVector2 GetPosition() const { return position_; }
    void SetPosition(const IntVector2& position) { position_ = position; }
    IntRect GetRect() const { return { position_ - size_ / 2, position_ + size_ / 2 }; }

    bool IsVisible()  const { return visible_ || size_.x_ == 0.f || size_.y_ == 0.f; }
    bool IsHovered()  const { return hovered_; }
    bool IsClicked()  const { return clicked_; }
    bool IsDragged()  const { return dragged_; }
    bool IsSelected() const { return selected_; }

    void SetVisible(bool visible) { visible_ = visible; }
    void SetHovered(bool hovered) { hovered_ = hovered; }
    void SetClicked(bool clicked) { clicked_ = clicked; }
    virtual void SetSelected(bool selected) { selected_ = selected; }

    bool AllowsDrag() { return allowDrag_; }
    virtual void SetDragged(bool dragged)
    {
        if (dragged)
            dragBegin_ = GetPosition();

        dragged_  = dragged;
    }
    IntVector2 GetDragBegin() const { return dragBegin_; }

protected:
    bool visible_;
    bool hovered_;
    bool clicked_;
    bool dragged_;
    bool selected_;
    bool allowDrag_;
    IntVector2 hoverPos_;
    IntVector2 clickBegin_;
    IntVector2 dragBegin_;

private:
    IntVector2 position_;
    IntVector2 size_;
    unsigned id_;
};

#endif // WIDGET_H
