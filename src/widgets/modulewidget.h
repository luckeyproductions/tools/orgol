/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef STREAMWIDGET_H
#define STREAMWIDGET_H

#include "nodulewidget.h"


class ModuleWidget: public NoduleWidget
{
public:
    ModuleWidget(Module* module);

    Module* GetModule() const { return static_cast<Module*>(nodule_); }

    void Draw(Painter* p) const override;
    void DrawPorts(Painter* p) const override;
    
    IntVector2 PortCoords(PortType type, unsigned index) const override;
    PODVector<IntVector2> GetInputCoords() const override;
    PODVector<IntVector2> GetOutputCoords() const override;
    PODVector<IntVector2> GetOutputPositions() const override;
    PODVector<IntVector2> GetInputPositions() const override;

    bool IsActiveModule() const;

private:
    void DrawInnerPorts(Painter* p) const;
};

#endif // STREAMWIDGET_H
