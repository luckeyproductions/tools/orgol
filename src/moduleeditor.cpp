/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "gui.h"
#include "widgets/modulewidget.h"
#include "widgets/connectorwidget.h"
#include "widgets/splitterwidget.h"
#include "widgets/multiplierwidget.h"
#include "widgets/gainwidget.h"
#include "widgets/clamperwidget.h"
#include "widgets/oscillatorwidget.h"
#include "widgets/stepperwidget.h"
#include "widgets/lifewidget.h"

#include "moduleeditor.h"

ModuleEditor* ModuleEditor::instance_{ nullptr };

ModuleEditor::ModuleEditor(Context* context): Object(context),
    toolBox_{},
    tweaker_{},
    scope_{},
    tooltip_{ nullptr },
    scene_{ nullptr },
    soundSource_{ nullptr },
    synthesizer_{ nullptr },
    activeNodule_{ nullptr },
    grid_{ 28 },
    hoverCell_{ M_MAX_INT, M_MAX_INT },
    hand_{},
    wiring_{ false },
    hair_(),
    dragging_{ false },
    splitterAtHand_{ false },
    rightHeld_{ false },
    links_{}
{
    instance_ = this;

    CreateTooltip();

    scene_ = context_->CreateObject<Scene>();
    Node* audioNode{ scene_->CreateChild("Audio") };
    soundSource_ = audioNode->CreateComponent<SoundSource>();
    synthesizer_ = audioNode->CreateComponent<Synthesizer>();
    soundSource_->Play(synthesizer_->GetStream());

    scope_.SetPolynomialType(1, PT_HARMONIC_SIN);
    scope_.SetCoefficients(0, { 0.f, GRAPHICS->GetWidth() });
    scope_.SetCoefficients(1, { GRAPHICS->GetHeight() - 2.f * grid_ });

    SubscribeToEvent(E_MOUSEBUTTONDOWN, DRY_HANDLER(ModuleEditor, HandleMouseButtonDown));
    SubscribeToEvent(E_MOUSEMOVE, DRY_HANDLER(ModuleEditor, HandleMouseMove));
    SubscribeToEvent(E_MOUSEWHEEL, DRY_HANDLER(ModuleEditor, HandleMouseWheel));
    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(ModuleEditor, HandleKeyDown));
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(ModuleEditor, UpdateTooltip));

    NewModule();
}

void ModuleEditor::CreateTooltip()
{
    tooltip_ = GetSubsystem<UI>()->GetRoot()->CreateChild<Text>();
    tooltip_->SetText("Test");
    tooltip_->SetFont(RES(Font, "Fonts/Philosopher.ttf"), 9);
    tooltip_->SetColor(Color::GRAY * 1.8f);
    tooltip_->SetEffectColor(Color::BLACK * 0.3f);
    tooltip_->SetTextEffect(TE_STROKE);
    tooltip_->SetTextAlignment(HA_LEFT);
    tooltip_->SetHorizontalAlignment(HA_LEFT);
    tooltip_->SetVerticalAlignment(VA_TOP);
    tooltip_->SetVisible(false);
}

void ModuleEditor::NewModule()
{
    new ModuleWidget{ GetSubsystem<ModuleMaster>()->NewModule() };
    synthesizer_->SetModule(GetActiveModule());

    {
        Nodule* oscillator{ GetActiveModule()->CreateOscillator(4u) };
        OscillatorWidget* oscillatorWidget{ new OscillatorWidget{ oscillator } };
        oscillatorWidget->SetCoords({ 7, 7 });
        Polynomial harmonic{ PT_HARMONIC_SIN };
        harmonic.SetCoefficients({ .0f, .3f, .1f, -.05f, .2f, -.1f  });
        harmonic.SetSlope({ 0.f, 23.f });
        oscillatorWidget->SetPolynomial(harmonic);

        Nodule* splitter{ GetActiveModule()->CreateSplitter() };
        SplitterWidget* splitterWidget{ new SplitterWidget{ splitter } };
        splitterWidget->SetCoords({ 7, 10 });

        Nodule* multiplier{ GetActiveModule()->CreateMultiplier() };
        MultiplierWidget* multiplierWidget{ new MultiplierWidget{ multiplier } };
        multiplierWidget->SetCoords({ 7, 13 });

        Nodule* gain{ GetActiveModule()->CreateGain() };
        GainWidget* gainWidget{ new GainWidget{ gain } };
        gainWidget->SetCoords({ 7, 16 });

        Nodule* clamper{ GetActiveModule()->CreateClamper() };
        ClamperWidget* clamperWidget{ new ClamperWidget{ clamper } };
        clamperWidget->SetCoords({ 7, 19 });

        Nodule* stepper{ GetActiveModule()->CreateStepper() };
        StepperWidget* stepperWidget{ new StepperWidget{ stepper } };
        stepperWidget->SetCoords({ 7, 22 });

        Nodule* life{ GetActiveModule()->CreateLife() };
        LifeWidget* lifeWidget{ new LifeWidget{ life } };
        lifeWidget->SetCoords({ 7, 25 });
    }
}

void ModuleEditor::Draw(Painter* p) const
{
    DrawGrid(p);
    DrawHand(p);

    for (const Link& l: links_)
        l.Draw(p);

    for (NoduleWidget* nw: GetNoduleWidgets(GetActiveModule()))
        nw->Draw(p);

    DrawHair(p);

    if(activeNodule_)
        DrawHUD(p);

}

void ModuleEditor::UpdateTooltip(StringHash eventType, VariantMap& eventData)
{
    NoduleWidgetPort port{ NoduleWidgetPortAt(hoverCell_) };

    if (port.first_ )
    {
        Nodule* nodule{ port.first_->GetNodule() };
        const unsigned portIndex{ port.second_.index_ };

        if (nodule && portIndex != M_MAX_UNSIGNED)
        {
            tooltip_->SetVisible(true);

            if (static_cast<Module*>(nodule) != GetActiveModule())
            {
                if (port.second_.type_ == PORT_IN)
                    tooltip_->SetText(String{ nodule->GetInputValue(portIndex) });
                else
                    tooltip_->SetText(String{ nodule->GetOutputValue(portIndex) });
            }
            else
            {
                if (port.second_.type_ == PORT_IN)
                    tooltip_->SetText(String{ nodule->GetOutputValue(portIndex) });
                else
                    tooltip_->SetText(String{ nodule->GetInputValue(portIndex) });
            }

            tooltip_->SetPosition(CoordsToPosition(hoverCell_) + IntVector2{ 5, -23 }
                                  + IntVector2::LEFT * 5 * (tooltip_->GetText().At(0) == '-'));

            return;
        }
    }

    tooltip_->SetVisible(false);
}

void ModuleEditor::DrawGrid(Painter* p) const
{
    const IntVector2 screenSize{ GRAPHICS->GetSize() };
    const Color gridColor{ Color::VIOLET.Transparent(.23f) };
    for (int x{ 0 }; x < screenSize.x_ / grid_ - 1; ++x)
    {
        const Vector2 from{ (x + 1.f) * grid_, .0f };
        const Vector2 to{   (x + 1.f) * grid_,  screenSize.y_ * 1.f };
        p->DrawLine(from, to, gridColor);
    }
    for (int y{ 0 }; y < screenSize.y_ / grid_; ++y)
    {
        const Vector2 from{             0.f, (y + 1.f) * grid_ };
        const Vector2 to{ screenSize.x_*1.f, (y + 1.f) * grid_ };
        p->DrawLine(from, to, gridColor);
    }
}

void ModuleEditor::DrawHand(Painter* p) const
{
    if (dragging_)
        return;

    if (hand_.type_ == NT_NONE)
    {
        const float g{ static_cast<float>(grid_) };
        p->DrawEllipse(Vector2{ CoordsToPosition(hand_.cell_) }, { g, g }, COL_CURSOR, 4);
    }
    else
    {
        const IntVector2 sizeBlocks{ *defaultSize[hand_.type_] };
        const Vector2 drawSize{ sizeBlocks * grid_ - *drawShrink[hand_.type_] };
        p->DrawPolynomial(NoduleWidget::Shape(hand_.type_,
                                              CoordsToPosition(hand_.cell_, sizeBlocks),
                                              drawSize, 1),
                          (hand_.valid_ ? COL_CURSOR : COL_INVALID), p->SizeToSegments(drawSize));
    }
}

void ModuleEditor::DrawHair(Painter* p) const
{
    if (IsWiring())
    {
        p->DrawLine( Vector2{ CoordsToPosition(hair_.from_) },
                     Vector2{ CoordsToPosition(hair_.to_) },
                     hair_.color_);
    }
}


void ModuleEditor::DrawHUD(Painter* p) const
{
    const IntVector2 screen{ GRAPHICS->GetSize() };
    const int hudHeight{ 13 * grid_ / 2 };
//    DrawEllipse({ screen.x_ / 2, screen.y_ - hudHeight / 2 + 3 * grid_ / 4 },
//                { screen.x_, hudHeight + grid_ },
//                Color::VIOLET.Transparent(.5f), 23, 1.23f);

//    if (activeNodule_)
//    {
//        DrawEllipse({ screen.x_ / 2, screen.y_ - hudHeight / 2 + 3 * grid_ / 4} ,
//                    { screen.x_ - grid_/2, hudHeight - grid_/3 + grid_ },
//                    Color::CHARTREUSE.Transparent(.42f), 23, 1.f);
//    }

//    renderer_->AddTriangle({ 0.f, screen.y_, 0.f },
//                           { screen.x_, screen.y_, 0.f },
//                           { 0.f, screen.y_ - hudHeight, 0.f },
//                           Color::VIOLET.Transparent(0.2f));
//    renderer_->AddTriangle({ screen.x_, screen.y_ - hudHeight, 0.f },
//                           { screen.x_, screen.y_, 0.f },
//                           { 0.f, screen.y_ - hudHeight, 0.f },
//                           Color::VIOLET.Transparent(.2f));

    p->DrawPolynomial(scope_, Color::AZURE.Transparent(.5f), 23);
}

void ModuleEditor::HandleMouseMove(StringHash eventType, VariantMap& eventData)
{
    const IntVector2 mousePos{ eventData[MouseMove::P_X].GetInt(),
                               eventData[MouseMove::P_Y].GetInt() };
    const bool middle{ (eventData[MouseMove::P_BUTTONS].GetInt() & MOUSEB_MIDDLE) != MOUSEB_NONE };

    hoverCell_ = mousePos / grid_;


    if (hand_.cell_ != hoverCell_ && middle)
        Laser();

    UpdateHand();
    UpdateHair();
}


void ModuleEditor::HandleMouseWheel(StringHash eventType, VariantMap& eventData)
{
    const int wheel{ eventData[MouseWheel::P_WHEEL].GetInt() };
    NoduleWidgetPort port{ NoduleWidgetPortAt(hoverCell_) };
    const unsigned portIndex{ port.second_.index_ };

    if (wheel != 0 && port.first_ && portIndex != M_MAX_UNSIGNED)
    {
        Nodule* nodule{ port.first_->GetNodule() };
        const float delta{ 0.1f * wheel };

        if (static_cast<Module*>(nodule) != GetActiveModule() && port.second_.type_ == PORT_IN)
        {
            const float oldVal{ nodule->GetInputValue(portIndex) };
            nodule->SetInputValue(portIndex, oldVal + delta);

        }
    }
}

void ModuleEditor::UpdateHand()
{
    const IntVector2 size{ (wiring_ ? *defaultSize[NT_SPLITTER]
                                    : *defaultSize[hand_.type_]) };
    hand_.cell_ = ClampCoords(hoverCell_, size);
    const bool empty{ CheckFree({ NoduleWidget::GetOccupy(
                                  hand_.cell_, size) }) };
    hand_.valid_ = empty;

    if (wiring_)
        hand_.type_ = splitterAtHand_ | empty ? NT_SPLITTER : NT_NONE;
}

void ModuleEditor::UpdateHair()
{
    hair_.to_ = hand_.cell_;
    PortType type{ PortTypeAt(hair_.to_) };

    bool same{ false };

    if (hair_.noduleWidget_ && NoduleWidgetsAt(hair_.to_).Contains(hair_.noduleWidget_))
        same = true;

    if (type == PORT_ALL || same)
    {
        hair_.color_ = Color::GRAY;
    }
    else
    { // Does not check for occupied inputs (yet)
        hair_.color_ = (PortMatch(hair_.fromPortType_, type) ? Color::GREEN
                                                             : Color::RED);
    }
}

bool ModuleEditor::PortMatch(PortType typeA, PortType typeB)
{
    if (typeA == PORT_ALL || typeB == PORT_ALL)
        return false;

    if (typeA == PORT_UNDEFINED || typeB == PORT_UNDEFINED)
        return true;

    if ((typeA == PORT_IN  && typeB == PORT_OUT)
     || (typeA == PORT_OUT && typeB == PORT_IN))
        return true;

    return false;
}

PortType ModuleEditor::PortTypeAt(const IntVector2& coords)
{
    return PortAt(coords).type_;
}

Port ModuleEditor::PortAt(const IntVector2& coords)
{
    return  NoduleWidgetPortAt(coords).second_;
}

NoduleWidgetPort ModuleEditor::NoduleWidgetPortAt(const IntVector2& coords)
{
    NoduleWidgetPort port{ nullptr, { PORT_ALL, M_MAX_UNSIGNED } };
    PODVector<NoduleWidget*> widgets{ NoduleWidgetsAt(coords) };

    // Output first
    for (NoduleWidget* nw: widgets)
    {
        Port outPort{ nw->PortAt(coords) };

        if (outPort.type_ == PORT_OUT)
            return { nw, outPort };
    }
    for (NoduleWidget* nw: widgets)
    {
        Port otherPort{ nw->PortAt(coords) };

        if (otherPort.type_ == PORT_UNDEFINED)
            return { nw, otherPort };
        else if (otherPort.type_ != PORT_ALL)
            port = { nw, otherPort };
    }

    return port;
}

void ModuleEditor::HandleMouseButtonDown(StringHash eventType, VariantMap& eventData)
{
    const unsigned buttons{ eventData[MouseButtonDown::P_BUTTONS].GetUInt() };

    const bool left{   (buttons & MOUSEB_LEFT )  != MOUSEB_NONE };
    const bool right{  (buttons & MOUSEB_RIGHT)  != MOUSEB_NONE };
    const bool middle{ (buttons & MOUSEB_MIDDLE) != MOUSEB_NONE };

    if (left && !right)
        HandleLeftClick();
    else if (right)
        HandleRightClick();
    else if (middle)
        HandleMiddleClick();

    UpdateHand();
}

void ModuleEditor::HandleLeftClick()
{
    // Place one last splitter.
    if (wiring_)
    {
        if (hand_.valid_)
            AddLink(PlaceNodule(NT_SPLITTER, hand_.cell_));
        else
        {
            for (NoduleWidget* nw: NoduleWidgetsAt(hand_.cell_))
            {
                if (PortMatch(nw->PortAt(hair_.to_).type_, hair_.fromPortType_))
                {
                    AddLink(nw);
                    break;
                }
            }
        }

        EndWiring();
    }
    // Add module.
    else if (hand_.valid_)
    {
        PlaceNodule(hand_.type_, hand_.cell_);
    }
}

void ModuleEditor::HandleRightClick()
{
    if (hand_.valid_)
    {
        HandleValidMouseRight();
    }
    else if (hand_.type_ <= NT_SPLITTER)
    {
        for (NoduleWidget* nw: NoduleWidgetsAt(hand_.cell_))
        {
            if (nw == hair_.noduleWidget_)
                continue;

            PODVector<IntVector2> inputCoords{  nw->GetInputCoords() };
            PODVector<IntVector2> outputCoords{ nw->GetOutputCoords() };

            if (wiring_)
            {
                if (PortMatch(nw->PortAt(hair_.to_).type_, hair_.fromPortType_))
                {
                    AddLink(nw);
                    EndWiring();
                }
            }
            else
            {
//                bool undefinedSplitter{ PortTypeAt(hand_.cell_) == PORT_UNDEFINED };

                // Prioritize outputs
                for (unsigned o{ 0 }; o < outputCoords.Size(); ++o)
                {
                    if (hand_.cell_ == outputCoords.At(o))
                    {
                        StartWiring();
                    }
                }

                if (!wiring_)
                {
                    for (unsigned i{ 0 }; i < inputCoords.Size(); ++i)
                    {
                        if (hand_.cell_ == inputCoords.At(i))
                        {
                            StartWiring();
                        }
                    }
                }
            }
        }
    }
}

void ModuleEditor::HandleValidMouseRight()
{
    if (wiring_ || hand_.type_ == NT_SPLITTER)
    {

        NoduleWidget* splitterWidget{ PlaceNodule(NT_SPLITTER, hand_.cell_) };

        if (wiring_)
            AddLink(splitterWidget);

//        PortType pt{ hair_.fromPortType_ == PORT_ALL
//                  || hair_.fromPortType_ == PORT_IN ? PORT_UNDEFINED
//                   : hair_.fromPortType_ };

        StartWiring();
    }
}

void ModuleEditor::HandleMiddleClick()
{
    if (wiring_)
        EndWiring();
    else if (hand_.cell_ == hoverCell_)
        Laser();
}

void ModuleEditor::Laser()
{
    DeleteAt(hoverCell_);
}

void ModuleEditor::StartWiring()
{
    NoduleWidgetPort widgetPort{ NoduleWidgetPortAt(hand_.cell_) };
    NoduleWidget* nw{ widgetPort.first_ };
    PortType portType{ widgetPort.second_.type_ };

    hair_.noduleWidget_ = nw;
    hair_.from_ = hand_.cell_;
    hair_.to_   = hand_.cell_;
    hair_.fromPortType_ = portType;

    wiring_ = true;
    splitterAtHand_ = hand_.type_ == NT_SPLITTER;
}

void ModuleEditor::EndWiring()
{
    wiring_ = false;
    hair_ = Hair{};

    hand_.type_ = splitterAtHand_ ? NT_SPLITTER
                                  : NT_NONE;
}

void ModuleEditor::HandleKeyDown(StringHash eventType, VariantMap& eventData)
{
    const unsigned key{ eventData[KeyDown::P_KEY].GetUInt() };

    switch (key)
    {       default: break;
    case KEY_ESCAPE: if (!wiring_) hand_.type_ = NT_NONE; break;
    case      KEY_1: hand_.type_ = NT_SPLITTER;   break;
    case      KEY_2: hand_.type_ = NT_MULTIPLIER; break;
    case      KEY_3: hand_.type_ = NT_GAIN;       break;
    case      KEY_4: hand_.type_ = NT_CLAMPER;    break;
    case      KEY_5: hand_.type_ = NT_OSCILLATOR; break;
    case      KEY_6: hand_.type_ = NT_STEPPER;    break;
    case      KEY_7: hand_.type_ = NT_LIFE;       break;
    case KEY_DELETE: DeleteSelection(); break;
    }

    if (wiring_)
        EndWiring();

    UpdateHand();
}

NoduleWidget* ModuleEditor::PlaceNodule(NoduleType type, const IntVector2 coords)
{
    NoduleWidget* noduleWidget{ nullptr };

    switch (type)
    {
    default: break;
    case NT_SPLITTER: {
        Nodule* splitter{ GetActiveModule()->CreateSplitter() };
        noduleWidget = new SplitterWidget{ splitter };
    } break;
    case NT_MULTIPLIER: {
        Nodule* multiplier{ GetActiveModule()->CreateMultiplier() };
        noduleWidget = new MultiplierWidget{ multiplier };
    } break;
    case NT_GAIN: {
        Nodule* gain{ GetActiveModule()->CreateGain() };
        noduleWidget = new GainWidget{ gain };
    } break;
    case NT_CLAMPER: {
        Nodule* clamper{ GetActiveModule()->CreateClamper() };
        noduleWidget = new ClamperWidget{ clamper };
    } break;
    case NT_OSCILLATOR: {
        Nodule* oscillator{ GetActiveModule()->CreateOscillator(2u) };
        noduleWidget = new OscillatorWidget{ oscillator };
    } break;
    case NT_STEPPER: {
        Nodule* stepper{ GetActiveModule()->CreateStepper() };
        noduleWidget = new StepperWidget{ stepper };
    } break;
    case NT_LIFE: {
        Nodule* life{ GetActiveModule()->CreateLife() };
        noduleWidget = new LifeWidget{ life };
    } break;
    }

    if (noduleWidget)
    {
        noduleWidget->SetCoords(coords);
        noduleWidget->SetHovered(true);
    }

    return noduleWidget;
}

void ModuleEditor::DeleteSelection()
{
    for (NoduleWidget* nw: GetNoduleWidgets(GetActiveModule()))
    {
        Nodule* n{ nw->GetNodule() };

        if (nw->IsSelected() && (!n || static_cast<Module*>(n) != GetActiveModule()))
        {
            nw->Remove();
            GUI::Get()->RemoveWidget(nw);
        }
    }
}

void ModuleEditor::DeleteAt(const IntVector2 coords)
{
    for (NoduleWidget* nw: NoduleWidgetsAt(coords))
    {
        Nodule* n{ nw->GetNodule() };
        if (!nw->IsDragged() && static_cast<Module*>(n) != GetActiveModule())
        {
            nw->Remove();
            GUI::Get()->RemoveWidget(nw);
        }
    }
}

void ModuleEditor::AddLink(NoduleWidget* noduleWidget)
{
    Port portA{ hair_.noduleWidget_->PortAt(hair_.from_) };
    Port portB{ noduleWidget->PortAt(hair_.to_) };

    links_.Push(Link{ hair_.noduleWidget_, portA,
                      noduleWidget,        portB });

    Link& link{ links_.Back() };

    if (link.Validate())
    {
        Module* module{ static_cast<Module*>(link.GetNoduleB()) };

        if (module && module == GetActiveModule())
        {
            module->Connect(link.GetPortB().index_,
                            link.GetNoduleA(), link.GetPortA().index_);
        }
        else
        {
            link.GetNoduleB()->Connect(link.GetPortB().index_,
                                       link.GetNoduleA(), link.GetPortA().index_);
        }
    }
}

void ModuleEditor::Select(Widget* widget, bool add)
{
    const PODVector<NoduleWidget*> selection{ GetSelection() };
    const bool remain{ selection.Size() > 1
                && selection.Contains(static_cast<NoduleWidget*>(widget))
                && !add };

    if (!remain)
        widget->SetSelected(!widget->IsSelected());
}
void ModuleEditor::Drag(Widget* widget)
{
    if (!widget->IsSelected())
    {
        for (NoduleWidget* nw: GetSelection())
            nw->SetSelected(false);

        widget->SetSelected(true);
    }
    else
    {
        for (NoduleWidget* nw: GetSelection())
            nw->SetDragged(true);
    }

    dragging_ |= true;
}

void ModuleEditor::Drop(Widget* widget)
{
    NoduleWidget* nw{ static_cast<NoduleWidget*>(widget) };

    if (nw)
    {
        if (!CheckFree(nw->GetOccupy(nw->PositionToCoords())))
            nw->SetPosition(nw->GetDragBegin());
        else
            nw->SetCoords(nw->PositionToCoords());
    }

    nw->SetDragged(false);
    dragging_ = false;
}

bool ModuleEditor::CheckFree(const HashSet<IntVector2>& cells)
{
    HashSet<IntVector2> occupied{};

    for (NoduleWidget* nw: GetNoduleWidgets(GetActiveModule()))
    {

        if (!nw->IsDragged())
        {
            for (const IntVector2& cell: nw->GetOccupied())
                occupied.Insert(cell);
        }
    }

    for (const IntVector2& c: cells)
    {
        if (occupied.Contains(c))
        {
            if (false/*EDITOR: Check for input/output match*/)
                continue;
            else
                return false;
        }
    }

    return true;
}

PODVector<NoduleWidget*> ModuleEditor::GetNoduleWidgets(Module* module) const
{
    PODVector<NoduleWidget*> noduleWidgets{};

    for (Widget* w: GUI::Get()->GetWidgets())
    {
        NoduleWidget* nw{ static_cast<NoduleWidget*>(w) };

        if (nw && (nw->GetNodule()->GetModule() == module ||
                   static_cast<Module*>(nw->GetNodule()) == module))
            noduleWidgets.Push(nw);
    }

    return noduleWidgets;
}

PODVector<NoduleWidget*> ModuleEditor::NoduleWidgetsAt(const IntVector2 coords) const
{
    PODVector<NoduleWidget*> widgets{};

    for (NoduleWidget* nw: GetNoduleWidgets(GetActiveModule()))
    {
        if (!nw->IsDragged() && nw->GetOccupied().Contains(coords))
            widgets.Push(nw);
    }

    return widgets;
}

NoduleWidget* ModuleEditor::NoduleWidgetAt(const IntVector2 coords) const
{
    return NoduleWidgetsAt(coords).Front();
}

PODVector<NoduleWidget*> ModuleEditor::GetSelection() const
{
    PODVector<NoduleWidget*> widgets{};

    for (NoduleWidget* nw: GetNoduleWidgets(GetActiveModule()))
    {
        if (nw->IsSelected())
            widgets.Push(nw);
    }

    return widgets;
}

IntVector2 ModuleEditor::CoordsToPosition(const IntVector2& coords, const IntVector2& size) const
{
    return coords * grid_ + grid_ / 2 * IntVector2::ONE * VectorMod(size, { 2, 2 });
}

IntVector2 ModuleEditor::PositionToCoords(const IntVector2 position, const IntVector2 size) const
{
    return ClampCoords((position + VectorMod(size + IntVector2::ONE, IntVector2{ 2, 2 }) * grid_ / 2) / grid_,
                       size);
}

IntVector2 ModuleEditor::ClampCoords(const IntVector2 coords, const IntVector2 size) const
{
    return VectorClamp(coords,
                       size / 2,
                       IntVector2{ /*!*/ 14 - (size.x_ - 1) / 2,
                                   /*!*/ 30 - (size.y_ - 1) / 2 });
}
