/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MODULEMASTER_H
#define MODULEMASTER_H

#include "luckey.h"
#include "Audio/Module/Module.h"


class ModuleMaster: public Object
{
    DRY_OBJECT(ModuleMaster, Object);

public:
    ModuleMaster(Context* context);

    Module* NewModule(unsigned inputs = 0, unsigned outputs = 1);

    Module* GetActiveModule() const { return activeModule_; }
    void SetActiveModule(Module* module) { activeModule_ = module; }

private:
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);
    bool SaveActiveModule();

    Vector<SharedPtr<Module> > openModules_;
    Module* activeModule_;
};

#endif // MODULEMASTER_H
