/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/



#include "gui.h"
#include "moduleeditor.h"

#include "mastercontrol.h"

DRY_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context *context): Application(context),
    scene_{ nullptr },
    synthesizer_{ nullptr }
{
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()
                                     ->GetAppPreferencesDir("luckey", "logs") + "Orgol.log";
    engineParameters_[EP_WINDOW_TITLE] = "Dry Orgol";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources";

    engineParameters_[EP_FULL_SCREEN] = false;
    engineParameters_[EP_WINDOW_WIDTH] = 420;
    engineParameters_[EP_WINDOW_HEIGHT] = 999;
//    engineParameters_[EP_WINDOW_RESIZABLE] = true;
    engineParameters_[EP_BORDERLESS] = true;

    Synthesizer::RegisterObject(context_);
    Nodule::RegisterObject(context_);
    Module::RegisterObject(context_);
}

void MasterControl::Start()
{
    Input* input{ GetSubsystem<Input>() };
    input->SetMouseMode(MM_ABSOLUTE);
    input->SetMouseVisible(true);

    context_->RegisterSubsystem<Painter>();
    context_->RegisterSubsystem<GUI>();
    context_->RegisterSubsystem<ModuleMaster>();
    context_->RegisterSubsystem<ModuleEditor>();

/*
    scene_ = context_->CreateObject<Scene>();
    synthesizer_ = scene_->CreateChild("Synthesizer")->CreateComponent<Synthesizer>();
    scene_->CreateComponent<SoundSource>()->Play(synthesizer_->GetStream());
//    scene_->CreateComponent<Nodule>();

    synthesizer_->CreateModule(false);
//    context_->RegisterSubsystem(synthesizer_);

    Module* module{ synthesizer_->GetModule() };
    module->Setup(false);

    Nodule* oscillator{ module->CreateOscillator(4u) };
    oscillator->SetPolynomial({ { .0f, .3f, .1f, -.05f, .2f, -.1f }, { 0.f, 50.f } });

    Nodule* stepper{ module->CreateStepper() };
    stepper->Connect(0, oscillator);
    stepper->SetInputValue(1u, 0.3f);
//    module->Connect(0, stepper);
//    oscillator->Connect(3u, stepper);

    Nodule* multiplier{ module->CreateMultiplier() };
    multiplier->Connect(0u, oscillator);
    multiplier->Connect(1u, oscillator);
    multiplier->Connect(2u, oscillator);
    multiplier->Connect(3u, oscillator);
    multiplier->Connect(4u, oscillator);
    multiplier->Connect(5u, oscillator);
    multiplier->Connect(6u, stepper);
        module->Connect(0, multiplier);
*/

    ENGINE->SetMaxFps(80);

//    GetSubsystem<ModuleEditor>()->NewModule();

}

void MasterControl::Stop()
{


    engine_->DumpResources(true);
}

void MasterControl::Exit()
{


    engine_->Exit();
}
