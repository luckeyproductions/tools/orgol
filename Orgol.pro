include(src/Orgol.pri)

TARGET = orgol

LIBS += \
    $${PWD}/Dry/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    Dry/include \
    Dry/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

HEADERS += \
    $$PWD/Audio/Module/Nodule.h \
    $$PWD/Audio/Module/Module.h \
    $$PWD/Audio/Module/ModuleDefs.h \
    $$PWD/Audio/Module/Synthesizer.h

SOURCES += \
    $$PWD/Audio/Module/Module.cpp \
    $$PWD/Audio/Module/Nodule.cpp \
    $$PWD/Audio/Module/Synthesizer.cpp

DISTFILES += \
    LICENSE_TEMPLATE

